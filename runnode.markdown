---
title: Running node
---

Prerequisite: Java 8 or higher ([OpenJDK](https://openjdk.java.net/install/), [Oracle Java](https://java.com/download/), or other).

* [Download](https://vasin.nl/blacknet-0.1.8.zip) and unzip latest release.
* Change directory to `blacknet/bin`
* On Windows run `blacknet.bat`
* On UN*X run `./blacknet`

Web interface is available at [http://localhost:8283/](http://localhost:8283/). Also see [HTTP API](apiv1.html).

To build and run development version:
```
git clone https://gitlab.com/blacknet-ninja/blacknet.git
cd blacknet
./gradlew run
```
