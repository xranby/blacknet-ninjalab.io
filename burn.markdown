---
title: Initial Burn Offering
---

Initial distribution will be proportional to burned BlackCoins, with a bonus for early participants.

For first 4 weeks the bonus is 20%, later it decreases by 2% per week.

The minimum required burnt amount per account is 10 BLK.

Burning starts at 07 Sep 2018 00:00:00 UTC and ends at 14 Dec 2018 00:00:00 UTC (14 weeks).

##The distribution
You can verify distribution using [genesis-tool](https://gitlab.com/blacknet-ninja/genesis-tool): `genesis-tool 2252350 2379599 > genesis.json`

<pre id="distribution">Loading...</pre>

<script type="text/javascript">
function getJSON(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
        var status = xhr.status;
        if (status === 200) {
          callback(null, xhr.response);
        } else {
          callback(status, xhr.response);
        }
    };
    xhr.send();
}
getJSON('/js/genesis.json',
function(err, data) {
    if (err !== null) {
        document.getElementById("distribution").innerHTML = "Network error: " + err;
        return;
    }

    document.getElementById("distribution").innerHTML = 'Last scanned block <a href="https://chainz.cryptoid.info/blk/block.dws?' + data["block"] + '.htm">' + data["block"] + "</a><br><br>";
    for (x in data) if (x != "block")
    document.getElementById("distribution").innerHTML += x + ' ' + data[x]/100000000 + "<br>";
});
</script>
