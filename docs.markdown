---
title: Documentation
---

- [Running node](runnode.html)
- [HTTP API](apiv1.html)
- [Anonymous connection to network](anonnet.html)
- [Initial distribution](burn.html)
