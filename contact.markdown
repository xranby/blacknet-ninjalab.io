---
title: Contact
---

[<i class="fas fa-comments"></i> #blacknet:matrix.org](https://riot.im/app/#/room/#blacknet:matrix.org)

[<i class="fab fa-bitcoin"></i> BitcoinTalk](https://bitcointalk.org/index.php?topic=469640.0)

[<i class="fab fa-gitlab"></i> GitLab](https://gitlab.com/blacknet-ninja)

[<i class="fas fa-rss-square"></i> Atom](atom.xml)
