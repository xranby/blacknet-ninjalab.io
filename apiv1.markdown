---
title: API Documentation
---

API version 1

API web-server listens on [http://localhost:8283/](http://localhost:8283/) It can be configured in config/ktor.conf

Path of all methods is prefixed with `/api/v1`

Encoding of values |
--- | ---
amount | [satoshis](https://en.bitcoin.it/wiki/Satoshi_(unit)) (1 coin = 100000000 satoshi)
data | JSON
hash | HEX-string
boolean | string "true" or "false"
string | UTF-8 string

Example of GET request: [http://localhost:8283/api/v1/nodeinfo](http://localhost:8283/api/v1/nodeinfo)

- Peers |
--- | ---
Type | GET
Path | /peerinfo
Returns data about each connected network node.

- Node info |
--- | ---
Type | GET
Path | /nodeinfo
Returns data about this node.

- Get block |
--- | ---
Type | GET
Path | /blockdb/get/{hash}/{txdetail?}
Arguments |
hash | hash of block
txdetail | true to include transaction data (optional boolean)
Returns data of a block with given block-hash.

- Get block hash |
--- | ---
Type | GET
Path | /blockdb/getblockhash/{height}
Arguments |
height | height of block
Returns hash of a block with given block-height.

- Ledger |
--- | ---
Type | GET
Path | /ledger
Returns data about current state of ledger.

- Account info |
--- | ---
Type | GET
Path | /ledger/get/{account}
Returns data of an account with given address.

- Transaction pool |
--- | ---
Type | GET
Path | /txpool
Returns data about memory pool of transactions.

- New account |
--- | ---
Type | GET
Path | /account/generate
Returns data of new account. Save mnemonic before using account.

- Mnemonic info |
--- | ---
Type | POST
Path | /mnemonic/info/{mnemonic}
Arguments |
mnemonic | mnemonic of account
Returns data about mnemonic: address and public key.

- Transfer |
--- | ---
Type | POST
Path | /transfer/{mnemonic}/{fee}/{amount}/{to}/{message?}/{encrypted?}
Arguments |
mnemonic | mnemonic of sending account
fee | transaction fee
amount | amount to transfer
to | address of receiving account
message | text message (optional)
encrypted | whether to encrypt message (optional)
Returns transaction hash.

- Burn |
--- | ---
Type | POST
Path | /api/v1/burn/{mnemonic}/{fee}/{amount}/{message?}
Arguments |
mnemonic | mnemonic of sending account
fee | transaction fee
amount | amount to burn
message | HEX-encoded message (optional)
Returns transaction hash.

- Lease |
--- | ---
Type | POST
Path | /lease/{mnemonic}/{fee}/{amount}/{to}
Arguments |
mnemonic | mnemonic of sending account
fee | transaction fee
amount | amount to lease
to | address of receiving account
Returns transaction hash.

- Cancel lease |
--- | ---
Type | POST
Path | /cancellease/{mnemonic}/{fee}/{amount}/{to}/{height}
Arguments |
mnemonic | mnemonic of sending account
fee | transaction fee
amount | leased amount
to | address of receiving account
height | height of lease
Returns transaction hash.

- Sign message |
--- | ---
Type | POST
Path | /signmessage/{mnemonic}/{message}
Arguments |
mnemonic | mnemonic of signing account
message | text message
Returns signature.

- Verify message |
--- | ---
Type | GET
Path | /verifymessage/{account}/{signature}/{message}
Arguments |
account | address of signing account
signature | signature to verify
message | text message
Returns boolean.

- Add peer |
--- | ---
Type | GET
Path | /api/v1/addpeer/{address}/{port?}
Arguments |
address | network address
port | network port (optional)
Returns string.

- Start staking |
--- | ---
Type | POST
Path | /staker/start/{mnemonic}
Arguments |
mnemonic | mnemonic of staking account
Returns boolean.

- Stop staking |
--- | ---
Type | POST
Path | /staker/stop/{mnemonic}
Arguments |
mnemonic | mnemonic of staking account
Returns boolean.
